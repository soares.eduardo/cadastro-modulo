//FUNÇÃO PARA DISPOR OS DADOS
function mostrarProdutosModulos(produto, modulo) {
    $('.mensagemErro').hide(); //ESCONDE UMA POSSIVEL MENSAGEM DE ERRO ANTERIOR
    $(".header-consulta").show();

    //VALIDAÇÃO DOS VALORES VINDOS DO DATASET
    if (produto == null || produto == undefined || produto == "") {
        produto = "Não identificado / Não especificado";
    }
    if (modulo == null || modulo == undefined || modulo == "") {
        modulo = "Não identificado / Não especificado";
    }

    //DISPOEM OS ELEMENTOS NA TABLE
    $("#tabelaBody").prepend(`
        <tr class="rowProduto">
            <td>${produto}</td>
            <td>${modulo}</td>
        </tr>
    `);
}
function buscaDataset() {
    var inputSelectProduto = $('#inputSelectProduto').val(); //PEGA O VALOR SELECIONADO
    var metads = [];

    //VERIFICA SE A OPÇÃO ORDENA PARA MOSTRAR TODOS OS RESULTADOS.
    if (inputSelectProduto == "todos") {
        var dataset = DatasetFactory.getDataset("dsModuloCadastro", null, null, null);
    } else {
        //GARANTE QUE A VARIAVEL NÃO VAI ENTRAR VAZIA
        if (inputSelectProduto != "" || inputSelectProduto != null || inputSelectProduto != undefined) {
            //CRIA A CONDIÇÃO DE SELEÇÃO.
            metads.push(DatasetFactory.createConstraint("inputProduto", inputSelectProduto, inputSelectProduto, ConstraintType.MUST));
        }
        metads.push(DatasetFactory.createConstraint("metadata#active", true, true, ConstraintType.MUST));
        //CONECTA NO DATASET PASSANDO A CONSTRAINT metads
        var dataset = DatasetFactory.getDataset("dsModuloCadastro", null, metads, null);
    }

    //VERIFICA SE O RETORNO DO DATASET POSSUI MAIS DO QUE ZERO RESULTADOS E ENTÃO ATRIBUI A VARIÁVEIS
    if (dataset.values.length > 0) {
        for (var i = 0; i < dataset.values.length; i++) {
            var produto = dataset.values[i]["inputProduto"];
            var modulo = dataset.values[i]["inputModulo"];
            mostrarProdutosModulos(produto, modulo);
        }
    }
}

//FUNÇÃO PARA FILTRAR A TABELA
$('#btnFiltrarModulos').click(function () {
    $('#tableConsultaModulo').DataTable().destroy();
    $(".rowProduto").remove(); //REMOVE AS POSSIVEIS ROWS ANTERIORES DA TABELA DE CONSULTA
    buscaDataset();

    $('#tableConsultaModulo').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
        }
    });
})

$(document).ready(function () {
    //ADICIONANDO "PLACEHOLDER" NO OPTION VAZIO
    $('#inputSelectProduto option').eq(0).text('Selecione um valor');

    //ADICIONA A OPÇÃO "TODOS" NO SELECT
    $('#inputSelectProduto').append(`
    <option value="todos">Todos</option>
    `);

    //TROCA O STATUS DO BOTÃO
    $("#inputSelectProduto").change(function () {
        if ($("#inputSelectProduto").val() == "") {
            $('#btnFiltrarModulos').attr('disabled', true);
        } else {
            $('#btnFiltrarModulos').attr('disabled', false);
        }
    });
});
