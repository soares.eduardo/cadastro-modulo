function setSelectedZoomItem(selectedItem) { //QUANDO O USUÁRIO SELECIONAR UM PRODUTO A TABELA IRÁ FILTRAR
    if (selectedItem.inputName == "inputProduto") {
        $("#inputModulo").attr("readonly", false);
    }
}

function removedZoomItem(removedItem) {
    if (removedItem.inputName == "inputProduto") {
        $("#inputModulo").attr("readonly", true);
    }
}

$(document).ready(function () {
    //ADICIONANDO READONLY 
    $("#inputModulo").attr("readonly", true);
});
