function validateForm(form) {
    var msg = "";

    if (form.getValue("inputProduto") != "" || form.getValue("inputProduto") != null) {
        var inputProduto = form.getValue("inputProduto");
        var inputModulo = form.getValue("inputModulo");
        var c1 = DatasetFactory.createConstraint('inputProduto', inputProduto, inputProduto, ConstraintType.MUST);

        var dataset = DatasetFactory.getDataset('dsModuloCadastro', null, [c1], null);
        if (dataset != undefined || dataset != null) {
            for (var i = 0; i < dataset.rowsCount; i++) {
                if (inputModulo.toUpperCase().trim() == dataset.getValue(i,'inputModulo').toUpperCase().trim()) {
                    msg = "\nMódulo " + dataset.getValue(i,'inputModulo')+ " já cadastrado.";
                }
            }

        }
    }

    if (form.getValue("inputProduto") == "" || form.getValue("inputProduto") == null) {
        msg += "Campo de produto não foi preenchido\n\n";
    }

    if (form.getValue("inputModulo") == "") {
        msg += "Campo de módulo não foi preenchido\n";
    }

    if (msg != "") {
        throw msg;
    }


}